# CI/CD for [sync-server](https://gitlab.com/sync-server)
|Folder/File|Description|
|-:|:-|
|`.basic-ci-cd.yml`|CI/CD for simple service|
|`.gitlab-ci.yml`|CI/CD to build docker images which will be handy during CI/CD|
|`Dockerfile-deploy`|Docker image for deploy stage|
